<?php
$nemos = array();
$nemos[] = 'BCOLOMBIA';
$nemos[] = 'BOGOTA';
$nemos[] = 'PFDAVVNDA';
$nemos[] = 'GRUPOAVAL';
$nemos[] = 'OCCIDENTE';
$nemos[] = 'PFHELMBANK';
?>
<table border="1" style="text-align: right;">
  <tr style="text-align: center;">
    <td>Nemo</td><td>Fecha</td><td>Hora</td><td>Precio</td><td>Volumen</td><td>Precio a&ntilde;o anterior</td><td>Variaci&oacute;n</td>
  </tr>
  <?php
  foreach ($nemos as $nemo) {
    $curlSession = curl_init("http://www.bvc.com.co/mercados/GraficosServlet?home=no&tipo=ACCION&nemo=$nemo&tiempo=ayer&1327776496703");
    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curlSession);
    curl_close($curlSession);
    // echo $response;
    $response = str_replace("\n", ',', $response);
    $response = str_replace(" ", ',', $response);
    // $datos = str_getcsv($response);
    $datos = explode(',', $response);
    echo "<tr>";
    echo '<td style="text-align: left;">';
    echo $nemo;
    echo "</td>";
    echo "<td>";
    echo $datos[0];
    echo "</td>";
    echo "<td>";
    echo $datos[1];
    echo "</td>";
    $precioActual = $datos[2];
    echo "<td>";
    echo number_format($precioActual, 2);
    echo "</td>";
    echo "<td>";
    echo $datos[3];
    echo "</td>";
    $datetime = new DateTime('now');
    $timestamp = $datetime -> format('U');
    $timestamp = $timestamp - 31536000;
    $precioAnhoAnterior = 0;
    foreach ($datos as $i => $dato) {
      if (($i % 5) == 0) {
        $datetimeDato = new DateTime($dato);
        $timestampDato = $datetimeDato -> format('U');
        if ($timestampDato <= $timestamp) {
          echo "<td>";
          $precioAnhoAnterior = $datos[$i + 2];
          echo number_format($precioAnhoAnterior, 2);
          echo "</td>";
          break;
        }
      }
    }
    $variacion = round((($precioActual - $precioAnhoAnterior) / $precioAnhoAnterior) * 100, 2);
    echo "<td>";
    echo $variacion . ' %';
    echo "</td>";
    // $datetime = new DateTime("@$timestamp");
    // $datetime -> setTimezone(new DateTimeZone('America/Bogota'));
    // echo $datetime -> format('Y-m-d');
    echo "</tr>";
  }
  ?>
</table>